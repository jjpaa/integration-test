// TDD - Unit testing

const exprect = require("chai").expect
const { expect } = require("chai");
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("RGB to Hex and Hex to RGB conversion", () => {
        it("converts the basic colors", () => {
            const redHex = converter.rgbToHex(255,0,0); // red ff0000
            const greenHex = converter.rgbToHex(0,255,0); // green 00ff00
            const blueHex = converter.rgbToHex(0,0,255); // green 0000ff

            expect(redHex).to.equal("ff0000")
            expect(greenHex).to.equal("00ff00")
            expect(blueHex).to.equal("0000ff");

            const rgb = converter.hexToRgb("ffffff");
            expect(rgb).to.equal("255255255")

        });
    });
});
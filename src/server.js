const express = require("express");
const converter = require("./converter");

const app = express();
const port = 3000;

app.get('/', (req, res) => res.send("Hello"));

app.get('/rgb-to-hex', (req, res) => {
    const red = parseInt(req.query.red, 10);
    const green = parseInt(req.query.green, 10);
    const blue = parseInt(req.query.blue, 10);
    const hex = converter.rgbToHex(red, green, blue);
    res.send(hex);
});

app.get('/hex-to-rgb', (req, res) => {
    const hex = req.query.hex;
    const rgb = converter.hexToRgb(hex);
    res.send(rgb);
});

if (process.env.NODE_ENV === 'test') {
    module.exports = app;
}
else {
    app.listen(port, () => console.log(`Server: localhost:${port}`));
}


/**
 * Padding outputs 2 characters always
 * @param {*} hex one or two characters
 * @returns {string} hex with two characters
 */
const pad = (hex) => {
    return(hex.length === 1 ? "0" + hex : hex)
}

module.exports = {
    /**
     * Convers RGB to Hex string
     * @param {number} red 0-255
     * @param {number} green 0-255
     * @param {number} blue 0-255
     * @returns {string} hex value
     */
    rgbToHex: (red, green, blue) => {
        // console.log({red, green, blue});
        const redHex = red.toString(16) // 0-255 -> 0-ff
        const greenHex = green.toString(16); // 0-255 -> 0-ff
        const blueHex = blue.toString(16); // 0-255 -> 0-ff
        const hex = pad(redHex) + pad(greenHex) + pad(blueHex);
        //console.log({hex});
        return hex; // hex string 6 characters
    },
    hexToRgb: (hex) => {
        let r = parseInt(hex.slice(0, 2), 16);
        let g = parseInt(hex.slice(2, 4), 16);
        let b = parseInt(hex.slice(4, 6), 16);
        return r.toString() + g.toString() + b.toString();
    }
}